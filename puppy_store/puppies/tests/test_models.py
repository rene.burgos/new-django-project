from django.test import TestCase
from ..models import Puppy, Dog, Kitten, Mutt

class PuppyTest(TestCase):
    """ Test module for Puppy model"""

    def setUp(self):
        Puppy.objects.create(
          name='Casper', age=3, breed='Bull Dog', color='Black'
        )

        Puppy.objects.create(
          name='Muffin', age=1, breed='Gradane', color='Brown'
        )

    def test_puppy_breed(self):
        puppy_casper = Puppy.objects.get(name='Casper')
        puppy_muffin = Puppy.objects.get(name='Muffin')

        self.assertEqual(
          puppy_casper.get_breed(), "Casper belongs to Bull Dog breed."
        )

        self.assertEqual(
          puppy_muffin.get_breed(), "Muffin belongs to Gradane breed."
        )

class DogTest(TestCase):
  """" Test module for Dog model"""

  def setUp(self):
        Dog.objects.create(
          name='Casper', age=3, breed='Bull Dog', color='Black'
        )

        Dog.objects.create(
          name='Muffin', age=1, breed='Gradane', color='Brown'
        )

    def test_eat_food(self):
        dog = Dog.objects.get(name='Casper')

        self.assertEqual(
          dog.eat_food(), 'munch munch....' 
        )

    def test_dog_breed(self):
        dog_casper = Dog.objects.get(name='Casper')
        dog_muffin = Dog.objects.get(name='Muffin')

        self.assertEqual(
          dog_casper.get_breed(), "Casper belongs to Bull Dog breed."
        )

        self.assertEqual(
          dog_muffin.get_breed(), "Muffin belongs to Gradane breed."
        )

class KittenTest(TestCase):
  """" Test module for Dog model"""

  def setUp(self):
        Kitten.objects.create(
          name='Casper', age=3, breed='Bull Dog', color='Black'
        )

        Kitten.objects.create(
          name='Muffin', age=1, breed='Gradane', color='Brown'
        )

    def test_dog_breed(self):
        cat_casper = Kitten.objects.get(name='Casper')
        cat_muffin = Kitten.objects.get(name='Muffin')

        self.assertEqual(
          cat_casper.get_breed(), "Casper belongs to Bull Dog breed."
        )

        self.assertEqual(
          cat_muffin.get_breed(), "Muffin belongs to Gradane breed."
        )


class MuttTest(TestCase):
  """" Test module for Dog model"""

  def setUp(self):
        Mutt.objects.create(
          name='Casper', age=3, breed='Bull Dog', color='Black'
        )

        Mutt.objects.create(
          name='Muffin', age=1, breed='Gradane', color='Brown'
        )

    def test_dog_breed(self):
        dog_casper = Mutt.objects.get(name='Casper')
        dog_muffin = Mutt.objects.get(name='Muffin')

        self.assertEqual(
          dog_casper.get_breed(), "Casper belongs to Bull Dog breed."
        )

        self.assertEqual(
          dog_muffin.get_breed(), "Muffin belongs to Gradane breed."
        )
